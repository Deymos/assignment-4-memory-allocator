#include "mem.h"
#include "tests.h"
#include <inttypes.h>

int64_t check_tests(test _tests[], size_t _tests_count) {
    int64_t tests_passed = 0;
    for (int64_t i = 0; i < _tests_count; i++) {
        bool result = _tests[i]();
        tests_passed += result;
        printf(" --- \t");
        printf( "Test #%" PRId64 " %s", i + 1, result ? "succeed" : "failed");
        printf(" \n");
    }
    return tests_passed;
}

int main() {
    int64_t tests_passed = check_tests(tests, tests_count);
    printf("\n --- \t");
    printf("Tests succeeded: %" PRId64 "/%" PRId64, tests_passed, tests_count);
    printf(" \n");
    return 0;
}
