#define _DEFAULT_SOURCE
#include "mem_internals.h"
#include "mem.h"
#include "tests.h"
#include <sys/mman.h>

#define OUTPUT stdout
#define HEAP_SIZE 12271
#define TEST_SIZE 2000
#define TEST4_MAX_SIZE 12272


static struct block_header* get_header(void* block) {
    return (struct block_header*) (block - offsetof(struct block_header, contents));
}

static void heap_free(void* heap) {
    munmap(heap, size_from_capacity((block_capacity){.bytes = HEAP_SIZE}).bytes);
}

bool test_success_malloc() {
    fprintf(OUTPUT, "\n\n --- Test test_success_malloc \n");
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(OUTPUT, heap);
    if (heap == NULL) {
        return false;
    }
    void* ptr = _malloc(TEST_SIZE);
    debug_heap(OUTPUT, heap);
    struct block_header *block = heap;
    if (ptr == NULL || block->capacity.bytes != TEST_SIZE) {
        return false;
    }
    _free(ptr);
    debug_heap(OUTPUT, heap);
    // free memory
    heap_free(heap);
    return true;
}

bool test_free_one() {
    fprintf(OUTPUT, "\n\n --- Test test_free_two \n");
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(OUTPUT, heap);
    void* block1 = _malloc(TEST_SIZE);
    void* block2 = _malloc(TEST_SIZE);
    void* block3 = _malloc(TEST_SIZE);
    struct block_header *block_header1 = get_header(block1);
    struct block_header *block_header2 = get_header(block2);
    struct block_header *block_header3 = get_header(block3);
    if (block1 == NULL || block_header1->capacity.bytes != TEST_SIZE) {
        return false;
    }
    if (block2 == NULL || block_header2->capacity.bytes != TEST_SIZE) {
        return false;
    }
    if (block3 == NULL || block_header3->capacity.bytes != TEST_SIZE) {
        return false;
    }
    debug_heap(OUTPUT, heap);
    _free(block2);
    if (block_header2->is_free == false) {
        return false;
    }
    debug_heap(OUTPUT, heap);
    // free memory
    _free(block3);
    _free(block1);
    heap_free(heap);
    return true;
}

bool test_free_two() {
    fprintf(OUTPUT, "\n\n --- Test test_free_two \n");
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(OUTPUT, heap);
    void* block1 = _malloc(TEST_SIZE);
    void* block2 = _malloc(TEST_SIZE);
    void* block3 = _malloc(TEST_SIZE);
    void* block4 = _malloc(TEST_SIZE);
    struct block_header *block_header1 = get_header(block1);
    struct block_header *block_header2 = get_header(block2);
    struct block_header *block_header3 = get_header(block3);
    struct block_header *block_header4 = get_header(block4);
    if (block1 == NULL || block_header1->capacity.bytes != TEST_SIZE) {
        return false;
    }
    if (block2 == NULL || block_header2->capacity.bytes != TEST_SIZE) {
        return false;
    }
    if (block3 == NULL || block_header3->capacity.bytes != TEST_SIZE) {
        return false;
    }
    if (block4 == NULL || block_header4->capacity.bytes != TEST_SIZE) {
        return false;
    }
    debug_heap(OUTPUT, heap);
    _free(block2);
    _free(block3);
    if (block_header2->is_free == false) {
        return false;
    }
    if (block_header3->is_free == false) {
        return false;
    }
    debug_heap(OUTPUT, heap);
    // free memory
    _free(block1);
    _free(block4);
    heap_free(heap);
    return true;
}

bool test_expand_region() {
    fprintf(OUTPUT, "\n\n --- Test test_expand_region \n");
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(OUTPUT, heap);
    void* block1 = _malloc(TEST4_MAX_SIZE);
    void* block2 = _malloc(TEST_SIZE);
    struct block_header *block_header1 = get_header(block1);
    struct block_header *block_header2 = get_header(block2);
    if (block1 == NULL || block_header1->capacity.bytes != TEST4_MAX_SIZE || block_header1->next != block_header2) {
        return false;
    }
    if (block2 == NULL || block_header2->capacity.bytes != TEST_SIZE) {
        return false;
    }
    // free memory
    debug_heap(OUTPUT, heap);
    _free(block1);
    _free(block2);
    heap_free(heap);
    return true;
}

bool test_expand_region_later() {
    fprintf(OUTPUT, "\n\n --- Test test_expand_region_later \n");
    void* heap = heap_init(HEAP_SIZE);
    debug_heap(OUTPUT, heap);
    void* block1 = _malloc(HEAP_SIZE);
    struct block_header *block_header1 = get_header(block1);
    debug_heap(OUTPUT, heap);
    void *after_heap = (void *) block_header1->contents + block_header1->capacity.bytes;
    void* _mapped = mmap(after_heap,
                         REGION_MIN_SIZE,
                         PROT_READ | PROT_WRITE,
                         MAP_PRIVATE | MAP_FIXED | MAP_ANONYMOUS,
                         -1,
                         0);
    if (_mapped == MAP_FAILED) {
        return false;
    }
    void* block2 = _malloc(TEST_SIZE);
    struct block_header *block_header2 = get_header(block2);
    if (block1 == NULL || block_header1->capacity.bytes != HEAP_SIZE || block_header1->next != block_header2) {
        return false;
    }
    if (block2 == NULL || block_header2->capacity.bytes != TEST_SIZE) {
        return false;
    }
    debug_heap(OUTPUT, heap);
    // free memory
    _free(block1);
    _free(block2);
    heap_free(heap);
    munmap(_mapped, REGION_MIN_SIZE);
    return true;
}

int64_t tests_count = 5;
test tests[] = {
        test_success_malloc,
        test_free_one,
        test_free_two,
        test_expand_region,
        test_expand_region_later
};
